<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWalletCategoriesTable extends Migration
{
    public function up()
    {
        Schema::create('wallet_categories', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid')->unique();
            $table->uuid('user_uuid');
            $table->uuid('parent_uuid')->nullable();
            $table->uuid('wallet_uuid');
            $table->uuid('category_uuid');
            $table->timestamps();
        });

        Schema::table('wallet_categories', function (Blueprint $table) {
            $table->foreign('user_uuid')->references('uuid')->on('users')->onDelete('cascade');
            $table->foreign('wallet_uuid')->references('uuid')->on('wallets')->onDelete('cascade');
            $table->foreign('category_uuid')->references('uuid')->on('categories')->onDelete('cascade');
            $table->foreign('parent_uuid')->references('uuid')->on('wallet_categories')->onDelete('cascade')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('wallet_categories');
    }
}

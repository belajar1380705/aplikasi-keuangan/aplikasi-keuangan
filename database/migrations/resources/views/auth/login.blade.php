<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Login untuk membeli voucher</title>
        <link href="{{ asset('assets/dashboard/css/styles.css') }}" rel="stylesheet" />
        <script src="https://use.fontawesome.com/releases/v6.3.0/js/all.js" crossorigin="anonymous"></script>
    </head>
    <body class="bg-login h-100">
        <div id="layoutAuthentication">
            <div id="layoutAuthentication_content">
                <main>
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-5">
                                <div class="card border-0 rounded-lg mt-5">
                                    <div class="card-header">
                                        <h4 class="text-center font-weight-light mt-4"><strong>Voucher</strong></h4>
                                        <p class="text-center small">Silakan Masuk untuk membeli voucher.</p>
                                    </div>
                                    <div class="card-body p-5">
                                        <form method="POST" action="{{ route('login') }}">
                                            @csrf
                                            <div class="row mb-3">
                                                <label for="email" class="col-md-12 mb-2"><strong>{{ __('Alamat E-mail') }}</strong></label>
                    
                                                <div class="col-md-12">
                                                    <input id="email" type="email" placeholder="Silakan masukkan alamat e-mail kamu" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                    
                                                    @error('email')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                    
                                            <div class="row mb-3">
                                                <label for="password" class="col-md-12 mb-2"><strong>{{ __('Kata Sandi') }}</strong></label>
                    
                                                <div class="col-md-12">
                                                    <input id="password" placeholder="Silakan masukkan kata sandi kamu" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                    
                                                    @error('password')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            
                                            <div class="row">
                                                <div class="col-md-12 mt-2">
                                                    <button type="submit" class="btn btn-secondary w-100">
                                                        {{ __('Masuk ke Dasbor') }}
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="card-footer text-center py-3">
                                        <div class="small"><a href="register.html">Belum punya akun ? daftar disini!</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
            {{-- <div id="layoutAuthentication_footer">
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid px-4">
                        <div class=" text-center small">
                            <div class="text-muted"><strong>Copyright 2023 &copy; Voucher</strong></div>
                        </div>
                    </div>
                </footer>
            </div> --}}
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="{{ asset('assets/dashboard/js/scripts.js') }}"></script>
    </body>
</html>

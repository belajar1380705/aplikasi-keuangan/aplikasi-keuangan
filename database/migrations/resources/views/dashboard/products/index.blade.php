@extends('layouts.dashboard-layout')
@section('content')
<div class="container-fluid px-4">
    
    <h1 class="mt-4">Produk</h1>
    @if (session('status'))
        <div class="alert alert-success">{{ session('status') }}</div>
    @endif
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active">List Produk</li>
    </ol>
    <div class="row">
        <div class="card mb-4">
            <div class="card-header">
                <i class="fas fa-table me-1"></i>
                DataTable Example
                <a href="" class="btn btn-sm btn-primary ml-3 text-white float-end" data-bs-toggle="modal" data-bs-target="#addModal">Tambah Produk</a>
            </div>
            <div class="card-body">
                <table id="datatablesSimple">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Produk</th>
                            <th>Harga</th>
                            <th>Tipe</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Nama Produk</th>
                            <th>Harga</th>
                            <th>Tipe</th>
                            <th>Aksi</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        @foreach ($products as $product)      
                            <tr>
                                <td>{{ $product->id }}</td>
                                <td>{{ $product->name }}</td>
                                <td>@currency($product->price) </td>
                                <td>{{ $product->type == '1' ? 'Unlimited':'Kuota' }}</td>
                                <td>
                                    <button class="btn btn-primary ml-3 text-white editBtn" data-bs-toggle="modal" data-bs-target="#editModal" value="{{ $product->id }}">Edit</button>
                                    <button class="btn btn-danger ml-3 text-white deleteBtn" data-bs-toggle="modal" data-bs-target="#deleteModal" value="{{ $product->id }}">Hapus</button>
                                    <a href="{{ url('/detail-product', $product->id) }}" class="btn btn-info ml-3 text-white">Detail</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Add Modal -->
<div class="modal fade" id="addModal" tabindex="-1" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="editModalLabel">Add Produk</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <form action="{{ url('store-product') }}" method="POST">
            @csrf
            <div class="modal-body">
                <div class="card-body">
                        <div class="col-md-12">
                            <div class="mb-3">
                                <label for="">Nama Produk: </label>
                                <input type="text" name="name" id="" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mb-3">
                                <label for="">Harga Produk: </label>
                                <input type="text" name="price" id="" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mb-3">
                                <label for="">Tipe Produk: </label>
                                <select name="type" id="" class="form-control">
                                    <option value="">--Pilih Tipe Produk--</option>
                                    <option value="0">Kuota</option>
                                    <option value="1">Unlimited</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mb-3">
                                <label for="">Deskripsi Produk: </label>
                                <textarea name="description" id="" rows="2"></textarea>
                            </div>
                        </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </form>
      </div>
    </div>
</div>
<!-- Edit Modal -->
<div class="modal fade" id="editModal" tabindex="-1" aria-labelledby="addModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="EditModalLabel">Edit Produk</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <form action="{{ url('update-product') }}" method="POST">
            @csrf
            @method('PUT')
            <input type="hidden" name="product_id" id="product_id" value="" />
            <div class="modal-body">
                <div class="card-body">
                        <div class="col-md-12">
                            <div class="mb-3">
                                <label for="">Nama Produk: </label>
                                <input type="text" name="name" id="name" class="form-control" value="">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mb-3">
                                <label for="">Harga Produk: </label>
                                <input type="text" name="price" id="price" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mb-3">
                                <label for="">Tipe Produk: </label>
                                <select name="type" id="type" class="form-control">
                                    <option value="">--Pilih Tipe Produk--</option>
                                    <option value="0">Kuota</option>
                                    <option value="1">Unlimited</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mb-3">
                                <label for="">Deskripsi Produk: </label>
                                <textarea name="description" id="description" rows="2"></textarea>
                            </div>
                        </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
        </form>
      </div>
    </div>
</div>
<!-- Delete Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="deleteModalLabel">Delete Produk</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <form action="{{ url('delete-product') }}" method="POST">
            @csrf
            @method('DELETE')
            <div class="modal-body">
                <div class="card-body">
                       <h4>Delete?</h4>
                       <input type="hidden" name="delete_product_id" id="deleting_id">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-warning">Yes</button>
            </div>
        </form>
      </div>
    </div>
</div>
@endsection
@section('scripts')
    <script>
        $(document).ready(function(){

            $(document).on('click', '.deleteBtn', function(){
                var product_id = $(this).val();
                // alert(product_id);
                $('#deleteModal').modal('show');
                $('#deleting_id').val(product_id);

            })

            $(document).on('click', '.editBtn', function(){
                var product_id = $(this).val();
                $('#editModal').modal('show');

                $.ajax({
                    type: "GET",
                    url:"/edit-product/"+product_id,
                    success: function (response){
                        // console.log(response.product.name);
                        $('#name').val(response.product.name);
                        $('#price').val(response.product.price);
                        $('#type').val(response.product.type);
                        $('#description').val(response.product.description);
                        $('#product_id').val(product_id);
                    }
                })
            });
        });
    </script>
@endsection
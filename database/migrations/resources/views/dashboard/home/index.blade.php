@extends('layouts.dashboard-layout')

@section('content')
<div class="container-fluid px-4">
    <h1 class="mt-4 text-center">Transaksi Kamu</h1>
    <p class="text-center">Lihat transaksi kamu disini!</p>
    {{-- <ol class="breadcrumb mb-4"> --}}
    {{-- </ol> --}}
    <br>
    @if (session('status'))
    <div class="alert alert-success">{{ session('status') }}</div>
    @endif
        <div class="card">
            {{-- <div class="card-header">
                Voucher
            </div> --}}
            <div class="card-body">
                <div class="row p-3">
                    <div class="col-lg-12 mb-3 border p-3">
                        <div class="form-group">
                            <select name="filter" id="filter" class="form-control">
                                <option value="">Harian</option>
                                <option value="">Bulanan</option>
                                <option value="">Tahunan</option>
                            </select>
                        </div>
                        <br>
                        <div class="form-group">
                            <div class="btn w-100 btn-primary">Tambah Transaksi</div>
                        </div>
                    </div>
                </div>
                <strong>01 Juni 2023</strong>
                <hr>
                <table width="100%" cellspacing="0" cellpadding="8">
                    <tr>
                        <td width="50px"><img src="{{ asset("assets/icon/category_1/icons-hamburger.png") }}" alt=""></td>
                        <td><small><strong>Makanan</strong></small><br>Beli Cilok</td>
                        <td class="text-success"><strong>+ @currency(25000)</strong></td>
                        <td>01 Juni 2023</td>
                    </tr>
                    <tr>
                        <td width="50px"><img src="{{ asset("assets/icon/category_1/icons-cash.png") }}" alt=""></td>
                        <td><small><strong>Makanan</strong></small><br>Beli Cilok</td>
                        <td class="text-danger"><strong>- @currency(25000)</strong></td>
                        <td>01 Juni 2023</td>
                    </tr>
                    <tr>
                        <td width="50px"><img src="{{ asset("assets/icon/category_1/icons-hamburger.png") }}" alt=""></td>
                        <td><small><strong>Makanan</strong></small><br>Beli Cilok</td>
                        <td class="text-success"><strong>+ @currency(25000)</strong></td>
                        <td>01 Juni 2023</td>
                    </tr>
                </table>
                <br>
                <strong>01 Juni 2023</strong>
                <hr>
                <table width="100%" cellspacing="0" cellpadding="8">
                    <tr>
                        <td width="50px"><img src="{{ asset("assets/icon/category_1/icons-hamburger.png") }}" alt=""></td>
                        <td><small><strong>Makanan</strong></small><br>Beli Cilok</td>
                        <td class="text-success"><strong>+ @currency(25000)</strong></td>
                        <td>01 Juni 2023</td>
                    </tr>
                    <tr>
                        <td width="50px"><img src="{{ asset("assets/icon/category_1/icons-cash.png") }}" alt=""></td>
                        <td><small><strong>Makanan</strong></small><br>Beli Cilok</td>
                        <td class="text-danger"><strong>- @currency(25000)</strong></td>
                        <td>01 Juni 2023</td>
                    </tr>
                    <tr>
                        <td width="50px"><img src="{{ asset("assets/icon/category_1/icons-hamburger.png") }}" alt=""></td>
                        <td><small><strong>Makanan</strong></small><br>Beli Cilok</td>
                        <td class="text-success"><strong>+ @currency(25000)</strong></td>
                        <td>01 Juni 2023</td>
                    </tr>
                </table>
            </div>
        </div>
</div>
<!-- Edit Modal -->
<div class="modal fade" id="editModal" tabindex="-1" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="editModalLabel">Edit Transaksi</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <form action="">
            <div class="modal-body">
                <div class="card-body">
                        <div class="col-md-12">
                            <div class="mb-3">
                                <label for="">Nama User: </label>
                                <input type="text" name="" id="" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mb-3">
                                <label for="">Harga Transaksi: </label>
                                <input type="text" name="" id="" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mb-3">
                                <label for="">Tipe Transaksi: </label>
                                <select name="" id="" class="form-control">
                                    <option value="">--Pilih Tipe Transaksi--</option>
                                    <option value="">A</option>
                                </select>
                            </div>
                        </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
        </form>
      </div>
    </div>
</div>
<!-- Delete Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="deleteModalLabel">Delete Transaksi</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <form action="">
            <div class="modal-body">
                <div class="card-body">
                       <h4>Delete?</h4>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-warning">Yes</button>
            </div>
        </form>
      </div>
    </div>
</div>
@endsection

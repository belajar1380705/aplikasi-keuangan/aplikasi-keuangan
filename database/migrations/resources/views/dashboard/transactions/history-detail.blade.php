@extends('layouts.dashboard-layout')
@section('content')
<div class="container-fluid px-4">

    <h1 class="mt-4">Transaksi Berhasil dibuat!</h1>
    @if(session('status'))
        <div class="alert alert-success">{{ session('status') }}</div>
    @endif

    <div class="row">
        <div class="card mb-4">
            <div class="card-header">
                <i class="fas fa-table me-1"></i>
                Success
            </div>
            <div class="row">


                <div class="col-lg-3">
                </div>
                <div class="col-lg-6">
                    <div class="card-body">
                        <table width="100%" cellpadding="8" cellspacing="0">
                            <tbody>
                                <tr>
                                    <td>Tanggal Transaksi</td>
                                    <td>{{ $history->transaction_date }}</td>
                                </tr>
                                <tr>
                                    <td>Transaction Id:</td>
                                    <td>{{ $history->transaction_id }}</td>
                                </tr>
                                <tr>
                                    <td>User Name: </td>
                                    <td>{{ $history->user->name }}</td>
                                </tr>
                                <tr>
                                    <td>Product Name: </td>
                                    <td>{{ $history->product->name }}</td>
                                </tr>
                                <tr>
                                    <td>Status: </td>
                                    <td>
                                        @if($history->status == '0')
                                            <span class="badge bg-warning">
                                                Menunggu Pembayaran
                                            </span>

                                        @elseif($history->status == '1')
                                            <span class="badge bg-success">
                                                Sukses
                                            </span>
                                        @else
                                            <span class="badge bg-danger">
                                                Gagal
                                            </span>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td>Voucher</td>
                                    <td>
                                        @if(is_null($history->code))
                                            -
                                        @else
                                            Code Voucher
                                        @endif
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
                <div class="col-lg-3">

                </div>
            </div>
            <p class="text-center text-bg-danger">Silahkan check transaksi secara berkala</p>
        </div>
    </div>


</div>
@endsection

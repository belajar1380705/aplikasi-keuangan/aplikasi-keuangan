@extends('layouts.dashboard-layout')
@section('content')
<div class="container-fluid px-4">
    
    <h1 class="mt-4">History</h1>
    @if (session('status'))
        <div class="alert alert-success">{{ session('status') }}</div>
    @endif
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active">List History</li>
    </ol>
    <div class="row">
        <div class="card mb-4">
            <div class="card-header">
                <i class="fas fa-table me-1"></i>
                DataTable Example
            </div>
            <div class="card-body">
                <table id="datatablesSimple">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama User</th>
                            <th>Nama Produk</th>
                            <th>Tanggal Transaksi</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Nama User</th>
                            <th>Nama Produk</th>
                            <th>Tanggal Transaksi</th>
                            <th>Aksi</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        @foreach ($histories as $history)      
                            <tr>
                                <td>{{ $history->id }}</td>
                                <td>{{ $history->user->name }}</td>
                                <td>{{ $history->product->name }}</td>
                                <td>{{ $history->transaction_date }} </td>
                                <td>
                                     <a href="{{ url('/detail-history', $history->id) }}" class="btn btn-info ml-3 text-white">Detail</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@extends('layouts.dashboard-layout')
@section('content')
<div class="container-fluid px-4">
    <h1 class="mt-4">Detail Produk</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active">Detail Produk </li>
    </ol>
    @if (session('status'))
        <div class="alert alert-success">{{ session('status') }}</div>
    @endif
    <div class="card">
        <div class="card-body">
            <div class="row">
                <p><b>{{ $history->user->name }}</b></p>
                <p><b>{{ $history->product->name }}</b></p> 
            </div>
            <div class="row">
                <div class="card mb-4">
                    <div class="card-header">
                        <i class="fas fa-table me-1"></i>
                        Detail List History
                    </div>
                    <div class="card-body">
                        <table id="datatablesSimple">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tipe Produk</th>
                                    <th>Kode Voucher</th>
                                    <th>Masa Berlaku</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Tipe Produk</th>
                                    <th>Kode Voucher</th>
                                    <th>Masa Berlaku</th>
                                    <th>Aksi</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                  
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <button class="btn btn-primary ml-3 text-white editBtn" data-bs-toggle="modal" data-bs-target="#editModal" ">Edit</button>
                                            <button class="btn btn-danger ml-3 text-white deleteBtn" data-bs-toggle="modal" data-bs-target="#deleteModal" ">Hapus</button>
                                        </td>
                                    </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
</div>
@endsection

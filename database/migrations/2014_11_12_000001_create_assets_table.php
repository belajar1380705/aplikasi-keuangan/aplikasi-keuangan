<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetsTable extends Migration
{
    public function up()
    {
        Schema::create('assets', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid')->after('id')->unique();
            $table->uuid('group_uuid');
            $table->string('name');
            $table->string('icon_url');
            $table->timestamps();
            $table->foreign('group_uuid')->references('uuid')->on('group_assets')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('assets');
    }
}

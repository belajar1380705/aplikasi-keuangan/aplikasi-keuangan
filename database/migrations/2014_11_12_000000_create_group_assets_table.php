<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupAssetsTable extends Migration
{
    public function up()
    {
        Schema::create('group_assets', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid')->after('id')->unique();
            $table->string('name');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('group_assets');
    }
}

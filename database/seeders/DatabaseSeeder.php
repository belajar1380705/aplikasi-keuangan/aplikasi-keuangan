<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Product;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        $this->call(GroupAssetTableSeeder::class);
        $this->call(AssetTableSeeder::class);
        $this->call(WalletTableSeeder::class);
        $this->call(CategoryTableSeeder::class);
    }
}

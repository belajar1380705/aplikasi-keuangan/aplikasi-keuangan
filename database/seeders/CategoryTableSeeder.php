<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Models\User;
use App\Models\Category;
use App\Models\WalletCategory;
use App\Models\Asset;
use App\Models\Wallet;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::where('id', 2)->first();
        $wallet = Wallet::where('id', 1)->first();
        $asset_one = Asset::where('id', 1)->first();
        $asset_two = Asset::where('id', 2)->first();

        $one = Category::create([
            'uuid' => Str::uuid()->toString(),
            'user_uuid' => $user->uuid,
            'name' => 'Gaji',
            'asset_uuid' => $asset_one->uuid,
            'type' => 1,
        ]);

        $two = Category::create([
            'uuid' => Str::uuid()->toString(),
            'user_uuid' => $user->uuid,
            'name' => 'Lemburan',
            'asset_uuid' => $asset_one->uuid,
            'type' => 1,
        ]);

        $three = Category::create([
            'uuid' => Str::uuid()->toString(),
            'user_uuid' => $user->uuid,
            'name' => 'Makanan',
            'asset_uuid' => $asset_two->uuid,
            'type' => 2,
        ]);

        $four = Category::create([
            'uuid' => Str::uuid()->toString(),
            'user_uuid' => $user->uuid,
            'name' => 'Minuman',
            'asset_uuid' => $asset_two->uuid,
            'type' => 2,
        ]);

        $wOne = WalletCategory::Create([
                'uuid' => Str::uuid()->toString(),
                'user_uuid' => $user->uuid,
                'parent_uuid' => null,
                'wallet_uuid' => $wallet->uuid,
                'category_uuid' => $one->uuid,
        ]);

        $wTwo = WalletCategory::Create([
                'uuid' => Str::uuid()->toString(),
                'user_uuid' => $user->uuid,
                'parent_uuid' => $wOne->uuid,
                'wallet_uuid' => $wallet->uuid,
                'category_uuid' => $two->uuid,
        ]);

        $wThree = WalletCategory::Create([
            'uuid' => Str::uuid()->toString(),
            'user_uuid' => $user->uuid,
            'parent_uuid' => null,
            'wallet_uuid' => $wallet->uuid,
            'category_uuid' => $three->uuid,
        ]);

        $wFour = WalletCategory::Create([
            'uuid' => Str::uuid()->toString(),
            'user_uuid' => $user->uuid,
            'parent_uuid' => null,
            'wallet_uuid' => $wallet->uuid,
            'category_uuid' => $four->uuid,
        ]);

        // Tambahkan data category lainnya sesuai kebutuhan
    }
}

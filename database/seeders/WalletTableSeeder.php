<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Wallet;
use App\Models\Asset;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class WalletTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::where('id', 2)->first();
        $asset = Asset::where('id', 2)->first();

        Wallet::create([
            'uuid' => Str::uuid()->toString(),
            'user_uuid' => $user->uuid,
            'asset_uuid' => $asset->uuid,
            'name' => 'Dompet Tunai',
            'type' => 1,
            'status' => 1,
        ]);
    }
}

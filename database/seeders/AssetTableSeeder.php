<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Asset;
use App\Models\GroupAsset;
use Illuminate\Support\Str;

class AssetTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $GroupAsset = GroupAsset::where('id', 1)->first();

        Asset::create([
            'uuid' => Str::uuid()->toString(),
            'group_uuid' => $GroupAsset->uuid,
            'name' => 'Kartu Bank',
            'icon_url' => 'assets/icon/category_1/icons-bank-card.png',
        ]);

        Asset::create([
            'uuid' => Str::uuid()->toString(),
            'group_uuid' => $GroupAsset->uuid,
            'name' => 'Uang Tunai',
            'icon_url' => 'assets/icon/category_1/icons-cash.png',
        ]);

        Asset::create([
            'uuid' => Str::uuid()->toString(),
            'group_uuid' => $GroupAsset->uuid,
            'name' => 'Internet',
            'icon_url' => 'assets/icon/category_1/icons-cellular-network.png',
        ]);

        // Tambahkan data asset lainnya sesuai kebutuhan
    }
}

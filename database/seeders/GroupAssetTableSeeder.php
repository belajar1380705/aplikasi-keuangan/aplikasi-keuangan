<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Models\GroupAsset;

class GroupAssetTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        GroupAsset::create([
            'uuid' => Str::uuid()->toString(),
            'name' => 'Group 1',
        ]);

        GroupAsset::create([
            'uuid' => Str::uuid()->toString(),
            'name' => 'Group 2',
        ]);

        // Tambahkan data group asset lainnya sesuai kebutuhan
    }
}

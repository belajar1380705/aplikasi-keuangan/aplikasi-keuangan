<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::group(['prefix' => 'v1'], function () {
    Route::post('/login', [App\Http\Controllers\API\Auth\LoginController::class, 'Login']);

    Route::group(['prefix' => 'assets', 'middleware' => ['auth:api']], function () {
        Route::get('', [App\Http\Controllers\API\Asset\AssetController::class, 'GetAssets']);
    });

    Route::group(['prefix' => 'wallets', 'middleware' => ['auth:api']], function () {
        Route::get('', [App\Http\Controllers\API\Wallet\WalletController::class, 'GetWallets']);
        Route::post('', [App\Http\Controllers\API\Wallet\WalletController::class, 'CreateWallet']);
        Route::put('/{uuid}', [App\Http\Controllers\API\Wallet\WalletController::class, 'UpdateWallet']);
        Route::delete('/{uuid}', [App\Http\Controllers\API\Wallet\WalletController::class, 'DeleteWallet']);
    });


    Route::group(['prefix' => 'categories', 'middleware' => ['auth:api']], function () {
        Route::get('', [App\Http\Controllers\API\Category\CategoryController::class, 'GetCategories']);
        Route::post('', [App\Http\Controllers\API\Category\CategoryController::class, 'CreateCategory']);
    });

});

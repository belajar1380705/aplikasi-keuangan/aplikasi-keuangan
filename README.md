## Requirement

- Composer Version 2.5.4
- php versi 8.2.7
- laravel versi 10.13.5
- database: postgresql

## How to Run this project ?

- composer install
- cp .env.example .env
- php artisan key:generate
- change database env variable value to yours.
- php artisan migrate --seed
- php artisan passport:install
- php artisan serve

## Tech Stack
- Laravel Requests
- Laravel Resources
- Laravel Try Catch
- Laravel Logging
- Laravel Passport

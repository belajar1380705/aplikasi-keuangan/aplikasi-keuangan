<?php

namespace App\Observers;

use App\Models\Wallet;

class WalletObserver
{
    /**
     * Handle the Wallet "creating" event.
     */
    public function Creating(Wallet $wallet): void
    {
        $wallet->uuid = \Str::uuid()->toString();
        $wallet->user_uuid = \Auth::user()->uuid;
    }
}

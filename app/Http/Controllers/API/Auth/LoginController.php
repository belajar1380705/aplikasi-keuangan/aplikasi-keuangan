<?php

namespace App\Http\Controllers\API\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Login\LoginRequest;
use App\Http\Resources\Requests\Login\LoginResourceRequest;
use App\Http\Resources\Responses\Login\LoginResourceResponse;

class LoginController extends Controller
{
    public function Login(LoginRequest $request)
    {
        // * Validate Requests
        $checkStruct = $this->ValidateStruct(new LoginResourceRequest($request), $request);
        if ($checkStruct['status'] == 'error') {
            return $this->errorResponse($checkStruct['message'], 400);
        }

        // Your login controller code here
        if (auth()->attempt($checkStruct['data'])) {
            // * Get User
            $user = auth()->user();

            // * Revoked User from oauth access token to avoid mutiple login sessions.
            \DB::Table('oauth_access_tokens')->where('user_id', $user->id)->update([
                'revoked' => true,
            ]);

            // * Generate New Access Token And Return it as a response.
            return $this->successResponse('Kamu berhasil login.', new LoginResourceResponse($user->createToken($request->email)), 200);
        }

        return $this->errorResponse('Invalid credentials', 401);
    }
}

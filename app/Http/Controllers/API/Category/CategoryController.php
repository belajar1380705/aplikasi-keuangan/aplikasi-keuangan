<?php

namespace App\Http\Controllers\API\Category;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\WalletCategory;
use App\Http\Requests\Categories\CreateCategoryRequest;
use App\Http\Resources\Requests\Categories\CreateCategoryResourceRequest;
use App\Http\Resources\Responses\Categories\GetCategoriesResponse;

class CategoryController extends Controller
{
    public function GetCategories()
    {
        $walletCategory = WalletCategory::whereNull('parent_uuid')->with(['category', 'children' => function ($query) {
            $query->with('category');
        }])->get();

        return $this->successResponse(
            'Berhasil mendapatkan Kategori.',
            GetCategoriesResponse::collection($walletCategory),
            200
        );
    }

    public function CreateCategory(CreateCategoryRequest $request)
    {
        // * Validate Requests
        $checkStruct = $this->ValidateStruct(new CreateCategoryResourceRequest($request), $request);
        if ($checkStruct['status'] == 'error') {
            return $this->errorResponse($checkStruct['message'], 400);
        }


        $check = Wallet::where('uuid', $request->wallet_uuid)->first();
        if (is_null($check)) {
            return $this->errorResponse('Dompet tidak ditemukan.', 400);
        }

        // * Update Data Wallet
        try {
            \DB::transaction(function () use ($request) {
                $category = Category::create([
                    'name' => $request->name,
                    'type' => 1,
                    'asset_uuid' => $request->asset_uuid
                ]);

                // WalletCategory::insert([

                // ])
            });

            return $this->successResponse("Berhasil mengubah dompet baru.", [], 200);
        } catch (\Throwable $th) {
            return $this->internalServerError($th);
        }
    }
}

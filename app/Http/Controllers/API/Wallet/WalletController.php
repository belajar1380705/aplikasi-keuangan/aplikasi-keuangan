<?php

namespace App\Http\Controllers\API\Wallet;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\Wallet;
use App\Http\Requests\Wallets\CreateWalletRequest;
use App\Http\Requests\Wallets\UpdateWalletRequest;
use App\Http\Resources\Responses\Wallets\GetWalletsResponse;
use App\Http\Resources\Requests\Wallets\CreateWalletResourceRequest;
use App\Http\Resources\Requests\Wallets\UpdateWalletResourceRequest;

class WalletController extends Controller
{
    public function GetWallets()
    {
        return $this->successResponse(
            'Berhasil mendapatkan wallet',
            GetWalletsResponse::collection(Wallet::where('user_uuid', \Auth::user()->uuid)->with('asset')->get()),
            200
        );
    }

    public function CreateWallet(CreateWalletRequest $request)
    {
        // * Validate Requests
        $checkStruct = $this->ValidateStruct(new CreateWalletResourceRequest($request), $request);
        if ($checkStruct['status'] == 'error') {
            return $this->errorResponse($checkStruct['message'], 400);
        }
        // * Create Data Wallet
        try {
            \DB::transaction(function () use ($request) {
                Wallet::create([
                    'asset_uuid' => $request->asset_uuid,
                    'name' => $request->name,
                    'type' => 1,
                    'status' => 1,
                ]);
            });

            return $this->successResponse("Berhasil membuat dompet baru.", [], 200);
        } catch (\Throwable $th) {
            return $this->internalServerError($th);
        }
    }

    public function UpdateWallet(UpdateWalletRequest $request, $uuid)
    {
        // * Validate Requests
        $checkStruct = $this->ValidateStruct(new UpdateWalletResourceRequest($request), $request);
        if ($checkStruct['status'] == 'error') {
            return $this->errorResponse($checkStruct['message'], 400);
        }

        $check = Wallet::where('uuid', $uuid)->first();
        if (is_null($check)) {
            return $this->errorResponse('Dompet tidak ditemukan.', 400);
        }

        // * Update Data Wallet
        try {
            \DB::transaction(function () use ($request, $uuid) {
                Wallet::where('uuid', $uuid)->update([
                    'name' => $request->name,
                    'type' => 1,
                    'status' => 1,
                ]);
            });

            return $this->successResponse("Berhasil mengubah dompet baru.", [], 200);
        } catch (\Throwable $th) {
            return $this->internalServerError($th);
        }
    }

    public function DeleteWallet($uuid)
    {
        $check = Wallet::where('uuid', $uuid)->first();
        if (is_null($check)) {
            return $this->errorResponse('Dompet tidak ditemukan.', 400);
        }

        // * Update Data Wallet
        try {
            \DB::transaction(function () use ($uuid) {
                Wallet::where('uuid', $uuid)->delete();
            });

            return $this->successResponse("Berhasil menghapus dompet.", [], 200);
        } catch (\Throwable $th) {
            return $this->internalServerError($th);
        }
    }
}

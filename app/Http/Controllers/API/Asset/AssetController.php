<?php

namespace App\Http\Controllers\API\Asset;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\GroupAsset;
use App\Http\Resources\Responses\GroupAssets\GroupAssetResponse;

class AssetController extends Controller
{
    public function GetAssets()
    {
        return $this->successResponse(
            'Berhasil mendapatkan asset',
            GroupAssetResponse::collection(GroupAsset::with('assets')->get()),
            200
        );
    }
}

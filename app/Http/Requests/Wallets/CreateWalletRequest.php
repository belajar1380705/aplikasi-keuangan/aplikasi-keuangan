<?php

namespace App\Http\Requests\Wallets;

use App\Http\Requests\BaseFormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class CreateWalletRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'required|min:4|max:20|regex:/^[a-zA-Z\s]+$/',
            'asset_uuid' => 'required|uuid',
        ];
    }

    public function messages(): array
    {
        return [
            'name.required' => 'Silakan isi nama terlebih dahulu.',
            'name.min' => 'Nama Minimal 4 Karakter.',
            'name.max' => 'Nama Maksimal 20 Karakter.',
            'name.regex' => 'Nama tidak boleh mengandung spesial karakter dan angka.',
            'asset_uuid.required' => 'Silakan isi asset uuid terlebih dahulu',
            'asset_uuid.uuid' => 'Silakan isi format uuid yang valid.'
        ];
    }
}

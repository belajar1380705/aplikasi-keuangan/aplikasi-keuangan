<?php

namespace App\Http\Resources\Responses\Categories;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class GetCategoriesResponse extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'uuid' => $this->uuid,
            'parent_uuid' => $this->parent_uuid,
            'name' => $this->category->name,
            'type' => $this->category->type,
            'asset_url' => asset($this->category->asset->icon_url),
            'balance' => 20000,
            'children' => GetCategoriesResponse::collection($this->children),
        ];
    }
}

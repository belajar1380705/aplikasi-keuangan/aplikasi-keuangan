<?php

namespace App\Http\Resources\Responses\GroupAssets;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Responses\Assets\AssetResponse;

class GroupAssetResponse extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'uuid' => $this->uuid,
            'name' => $this->name,
            'assets' => AssetResponse::collection($this->assets)
        ];
    }
}

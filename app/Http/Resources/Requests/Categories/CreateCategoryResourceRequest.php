<?php

namespace App\Http\Resources\Requests\Categories;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CreateCategoryResourceRequest extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'name' => $this->name,
            'asset_uuid' => $this->asset_uuid,
            'parent_uuid' => is_null($this->parent_uuid) ? $this->parent_uuid : null,
            'wallet_uuid' => $this->wallet_uuid,
            'type' => $this->type,
        ];
    }
}

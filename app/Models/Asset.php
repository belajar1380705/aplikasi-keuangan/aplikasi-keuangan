<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{
    use HasFactory;

    protected $fillable = ['uuid', 'group_uuid', 'name', 'icon_url'];

    public function groupAsset()
    {
        return $this->belongsTo(GroupAsset::class, 'group_uuid', 'uuid');
    }

    public function wallet()
    {
        return $this->belongsTo(Wallet::class, 'wallet_uuid', 'uuid');
    }
}

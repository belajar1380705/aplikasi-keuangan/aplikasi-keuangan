<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    use HasFactory;

    protected $fillable = [
        'uuid',
        'user_uuid',
        'asset_uuid',
        'name',
        'type',
        'status',
    ];

    public function groupAsset()
    {
        return $this->belongsTo(GroupAsset::class, 'group_uuid', 'uuid');
    }

    public function asset()
    {
        return $this->belongsTo(Asset::class, 'asset_uuid', 'uuid');
    }

    public function walletCategories()
    {
        return $this->hasMany(WalletCategory::class, 'wallet_uuid', 'uuid');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GroupAsset extends Model
{
    use HasFactory;

    protected $fillable = ['uuid', 'name'];

    public function assets()
    {
        return $this->hasMany(Asset::class, 'group_uuid', 'uuid');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WalletCategory extends Model
{
    use HasFactory;

    protected $fillable = ['uuid', 'user_uuid', 'wallet_uuid', 'parent_uuid', 'category_uuid'];

    public function wallet()
    {
        return $this->belongsTo(Wallet::class, 'wallet_uuid', 'uuid');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_uuid', 'uuid');
    }

    public function parent()
    {
        return $this->belongsTo(WalletCategory::class, 'parent_uuid', 'uuid');
    }

    public function children()
    {
        return $this->hasMany(WalletCategory::class, 'parent_uuid', 'uuid');
    }
}

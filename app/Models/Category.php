<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $fillable = ['uuid', 'name', 'user_uuid', 'asset_uuid', 'type'];

    public function walletCategories()
    {
        return $this->hasMany(WalletCategory::class, 'wallet_uuid', 'uuid');
    }

    public function asset()
    {
        return $this->belongsTo(Asset::class, 'asset_uuid', 'uuid');
    }
}

<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

trait ValidateRequest
{
    protected function ValidateStruct($credentials, $request)
    {
        // * Validate Requests
        $credentials = $credentials->toArray($request);
        $req = $request->toArray();

        ksort($credentials);
        ksort($req);

        if ($credentials !== $req) {
            return [
                'status' => 'error',
                'message' => "Permintaan Tidak Valid"
            ];
        }

        return [
            'status' => 'success',
            'data' => $credentials,
        ];
    }
}

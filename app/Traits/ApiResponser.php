<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

trait ApiResponser
{
    protected function internalServerError($error)
    {
        \Log::error($error->getMessage());
        return $this->errorResponse("Server sedang mengalami gangguan internal", 500);
    }

    protected function errorResponse($message, $code, $data = [])
    {
        return response()->json(['meta' => ['code' => $code, 'status' => 'error', 'message' => $message], 'data' => $data], $code);
    }

    protected function successResponse($message, $data, $code)
    {
        return response()->json(['meta' => ['code' => $code, 'status' => 'success', 'message' => $message], 'data' => $data], $code);
    }

    /**
     * Build success response
     * @param  string|array $data
     * @param  int $code
     * @return Illuminate\Http\JsonResponse
     */
    public function paginateResponse($data, $code = 200, $message = 'success')
    {
        $pagination = [
            'total' => $data->total(),
            'count' => $data->lastItem(),
            'per_page' => (int) $data->perPage(),
            'current_page' => $data->currentPage(),
            'total_pages' => $data->lastPage(),
            'links' => [
                'first_page' => $data->url(1),
                'last_page' => $data->url($data->lastPage()),
                'next_page' => $data->nextPageUrl(),
                'prev_page' => $data->previousPageUrl(),
            ],
        ];
        return response()->json(['meta' => ['code' => $code, 'status' => 'success', 'message' => $message], 'data' => $data->items(), 'pagination' => $pagination], $code);
    }
}
